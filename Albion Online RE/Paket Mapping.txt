Bytes (decimal) | Description
--------------------
0-41: UDP Header
42-45: Some Kind of Header
46-47: Some Kind of Login Session Info? Does not change in same Login
48-53: Unknown, Changes between packages

[Possible Packet Payload Start]
54: Seems to be some kind of package identifier, All marketplace browse/create buy Data packages got the value (08) 
55: Same between all observed packages, just (00)
56-59: Is the same between all observed packages, some kind of flag? (01 00 00 00)
60-61: Is the size of the data between bytes 54-end
62-63: Again same in all packets, just (00 00)
64: The same in all viewed market related packets (08)
65-71: Changes between packets, Unknown
72-75: Looks like some kind of Flag, changes with some requests
76-77: Packet counter, goes up for every subsequent packet
78-79: Again same in all packets, just (00 00)
80-83: Is the same for all packets of this transaction
84-85: Keeps changing for every packet, Unknown

[Packet with Packet counter 00 && not last packet]
86-100: Changes, Unknown
101-end: Json Marketplace Data

[Packet with Packet counter >00 && not last packet]
86-end: Json Marketplace Data

[Last packet]
Json Terminating bytes: end signature (fd 6b 00), there may be more data behind it!