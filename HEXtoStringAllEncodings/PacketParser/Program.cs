﻿

using ExitGames.Client.Photon;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PacketParser
{
    public class MarketPacketParams
    {
        public Int64 lSomeLong; // param 1
        public string sCategory;
        public string sSubCategory;
        public int eEnchantmentFilterType;
        public string sEnchantmentLevel;
        public int eQualityFilterType;
        public string sQualityFilterType;
        public string sTier;
        public short aiItemTypes;
        public bool bAscending;
        public bool alwaysTrue = true;
        public int iMaxResults = 50;
        public int opcode = 67;

        public void PrintValues()
        {
            Type type = GetType();

            foreach (var f in type.GetFields().Where(f => f.IsPublic))
            {
                Console.WriteLine("Name: " + f.Name + " Value: " + f.GetValue(this));
            }
        }
    }

    class Program
    {
        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string ParseStringParam(BinaryReader binRead)
        {
            byte currentParam = binRead.ReadByte();
            char currentValueType = binRead.ReadChar();
            binRead.ReadByte(); // empty val
            byte valueLength = binRead.ReadByte();
            if (valueLength > 0)
            {
                return new string(binRead.ReadChars(valueLength));
            }

            return "";
        }

        public static Int64 ParseLongParam(BinaryReader binRead)
        {
            byte currentParam = binRead.ReadByte();
            char currentValueType = binRead.ReadChar();

            return binRead.ReadInt64();
        }

        public static int ParseIntParam(BinaryReader binRead)
        {
            byte currentParam = binRead.ReadByte();
            char currentValueType = binRead.ReadChar();

            return binRead.ReadInt32();
        }

        public static uint ParseUIntParam(BinaryReader binRead)
        {
            byte currentParam = binRead.ReadByte();
            char currentValueType = binRead.ReadChar();

            return binRead.ReadUInt32();
        }

        public static bool ParseBoolParam(BinaryReader binRead)
        {
            byte currentParam = binRead.ReadByte();
            char currentValueType = binRead.ReadChar();

            return binRead.ReadByte() > 0;
        }

        public static void SkipStupidSeventhParam(BinaryReader binRead)
        {
            byte nextParam = 0;
            while (nextParam != 7)
            {
                nextParam = (byte)binRead.PeekChar();
                if (nextParam == 7)
                {
                    break;
                }

                binRead.ReadByte();
            }
        }

        public static MarketPacketParams ParseMarketPacketParams(byte[] bytes)
        {
            MemoryStream memStr = new MemoryStream(bytes);
            BinaryReader binRead = new BinaryReader(memStr);

            MarketPacketParams ret = new MarketPacketParams();

            // Param 00
            ret.lSomeLong = ParseLongParam(binRead);

            // Param 01
            ret.sCategory = ParseStringParam(binRead);

            // Param 02
            ret.sSubCategory = ParseStringParam(binRead);

            // Param 03
            ret.sQualityFilterType = ParseStringParam(binRead);

            // Param 04
            ret.eEnchantmentFilterType = ParseIntParam(binRead);

            // Param 05
            ret.sTier = ParseStringParam(binRead);

            // Param 06
            SkipStupidSeventhParam(binRead);

            // Param 07
            ParseIntParam(binRead);

            // Param 08
            ret.sEnchantmentLevel = ParseStringParam(binRead);

            // Param 09
            binRead.ReadByte();
            binRead.ReadChar();
            binRead.ReadBytes(3);
            ret.iMaxResults = binRead.ReadByte();

            // Param 0a
            ret.alwaysTrue = ParseBoolParam(binRead);

            // Param 0b
            ret.alwaysTrue = ParseBoolParam(binRead);

            binRead.ReadBytes(3);

            ret.opcode = binRead.ReadByte();

            return ret;
        }

        struct pp
        {
            public short paramID;
            public byte paramType;
            public object value;
        }

        static void Main(string[] args)
        {
            // Takes Param hex code as first param 
            byte[] dBytes = StringToByteArray(args[0]);
            StreamBuffer sb = new StreamBuffer(dBytes);
            for (int i = 0; i < 28; i++)
            {
                sb.ReadByte();
            }

            Protocol16 p = new Protocol16();

            List<pp> d = new List<pp>();
            var paramID00 = p.DeserializeShort(sb);
            var paramType = p.DeserializeByte(sb);
            var iv = p.Deserialize(sb, paramType);

            d.Add(new pp() { paramID = paramID00, paramType = paramType, value = iv });

            sb.ReadByte();
            paramType = p.DeserializeByte(sb);
            iv = p.Deserialize(sb, paramType);




            MarketPacketParams par = ParseMarketPacketParams(dBytes);
            par.PrintValues();
            Console.Read();
        }

        private static string Obscure(string s)
        {
            while (s.Length < a.Length)
            {
                s += "\0";
            }
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] += a[i % a.Length];
            }
            return Convert.ToBase64String(bytes);
        }

        private static string Unobscure(string s)
        {
            byte[] array = Convert.FromBase64String(s);
            for (int i = 0; i < array.Length; i++)
            {
                array[i] -= a[i % a.Length];
            }
            string @string = Encoding.UTF8.GetString(array);
            return @string.TrimEnd(new char[1]);
        }

        private static byte[] a = new byte[]
{
        227,
        23,
        32,
        97,
        115,
        143,
        92,
        197,
        25,
        250,
        75,
        83,
        58,
        148,
        137,
        102,
        60,
        45,
        140,
        217,
        19,
        170,
        215,
        182,
        140,
        101,
        211,
        100,
        191,
        10,
        149,
        72,
        122,
        255,
        169,
        225,
        202,
        196,
        220,
        178,
        1,
        238,
        203,
        143,
        102,
        25,
        103,
        164,
        70,
        160
};

    }
}
