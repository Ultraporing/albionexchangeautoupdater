﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AOPC_Test
{
    public static class Helper
    {
        public static string ToPrettyString<Type>(this IList<Type> list)
        {
            var str = new StringBuilder();
            str.Append("{\n");
            int idx = 0;
            foreach (var s in list)
            {
                str.Append(String.Format(" {0}={1}\n", idx, s));
                idx++;
            }
            str.Append("}");
            return str.ToString();
        }

        public static string ToPrettyString<TKey>(this IDictionary<TKey, List<System.Type>> dict)
        {
            var str = new StringBuilder();
            str.Append("{\n");
            foreach (var pair in dict)
            {
                string[] the_array = pair.Value.Select(i => i.ToString()).ToArray();
                str.Append(String.Format(" {0}={1}\n", pair.Key, string.Join(";", the_array)));
            }
            str.Append("}");
            return str.ToString();
        }

        public static string ToPrettyString<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        {
            var str = new StringBuilder();
            str.Append("{\n");
            foreach (var pair in dict)
            {
                str.Append(String.Format(" {0}={1}\n", pair.Key, pair.Value));
            }
            str.Append("}");
            return str.ToString();
        }

        public static string ToPrettyString(this IDictionary<ajf, Type> dict)
        {
            var str = new StringBuilder();
            str.Append("{\n");
            foreach (var pair in dict)
            {
                str.Append(String.Format(" {0}={1}\n", (int)pair.Key, pair.Value));
            }
            str.Append("}");
            return str.ToString();
        }

        public static string ToPrettyString(this IDictionary<ajf, List<System.Type>> dict)
        {
            var str = new StringBuilder();
            str.Append("{\n");
            foreach (var pair in dict)
            {
                string[] the_array = pair.Value.Select(i => i.ToString()).ToArray();
                str.Append(String.Format(" {0}={1}\n", (int)pair.Key, string.Join(";", the_array)));
            }
            str.Append("}");
            return str.ToString();
        }
    }
}
