﻿using Albion.Common.Photon;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace AOPC_Test
{

    class Program
    {


        static void Main(string[] args)
        {
            ara.a();
            Dictionary<OperationCodes, System.Type> privateFieldA = typeof(ara).GetField("a", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null) as Dictionary<OperationCodes, System.Type>;
            Dictionary<OperationCodes, List<System.Type>> privateFieldB = typeof(ara).GetField("b", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null) as Dictionary<OperationCodes, List<System.Type>>;
            Dictionary<ajf, System.Type> privateFieldC = typeof(ara).GetField("c", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null) as Dictionary<ajf, System.Type>;
            Dictionary<ajf, List<System.Type>> privateFieldD = typeof(ara).GetField("d", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null) as Dictionary<ajf, List<System.Type>>;
            MethodInfo privateFieldE = typeof(ara).GetField("e", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null) as MethodInfo;
            MethodInfo privateFieldF = typeof(ara).GetField("f", BindingFlags.Static | BindingFlags.NonPublic).GetValue(null) as MethodInfo;
            List<string> publicFieldG = typeof(ara).GetField("g", BindingFlags.Static | BindingFlags.Public).GetValue(null) as List<string>;

            string fieldA = "Field: ara.a, Type: Dictionary<OperationCodes, System.Type>:\n" + privateFieldA.ToPrettyString() + "\n";
            string fieldB = "Field: ara.b, Type: Dictionary<OperationCodes, List<System.Type>>:\n" + privateFieldB.ToPrettyString() + "\n";
            string fieldC = "Field: ara.c, Type: Dictionary<ajf, System.Type>:\n" + privateFieldC.ToPrettyString() + "\n";
            string fieldD = "Field: ara.d, Type: Dictionary<ajf, List<System.Type>>:\n" + privateFieldD.ToPrettyString() + "\n";
            string fieldG = "Field: ara.g, Type: List<string>:\n" + publicFieldG.ToPrettyString();
            Console.Write(fieldA + fieldB + fieldC + fieldD + fieldG);
            var bla = typeof(ara).GetMethod("DecodeEvent");

        }
    }
}
