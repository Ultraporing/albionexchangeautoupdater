﻿using System;
using System.IO;
using System.Text;

namespace HEXtoStringAllEncodings
{
    class Program
    {
        public static string toLittleEndian(String hex)
        {
            String hexLittleEndian = "";
            if (hex.Length % 2 != 0) return "";
            for (int i = hex.Length - 2; i >= 0; i -= 2)
            {
                hexLittleEndian += hex.Substring(i, 2);
            }

            return hexLittleEndian;
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }


        // Dumps the strings into the dump.txt, Runs through all offsets
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("HexToString <Hex_without_space> [outputfile]");
                Console.ReadKey();
                return;
            }

            string filename = "dump.txt";
            if (args.Length == 2)
            {
                filename = args[1];
            }

            File.Delete(filename);
            using (StreamWriter file = new StreamWriter(filename))
            {
                // It takes the hexcode as parameter
                byte[] dBytes = StringToByteArray(args[0]);
                byte[] dLBytes = StringToByteArray(toLittleEndian(args[0]));

                //To get ASCII value of the hex string.
                file.WriteLine("[BE] Showing value in ASCII: " + Encoding.ASCII.GetString(dBytes));
                file.WriteLine("[LE] Showing value in ASCII: " + Encoding.ASCII.GetString(dLBytes));

                //To get the UTF7 endian value of the hex string
                file.WriteLine("[BE] Showing value in UTF7: " + Encoding.UTF7.GetString(dBytes));
                file.WriteLine("[LE] Showing value in UTF7: " + Encoding.UTF7.GetString(dLBytes));

                //To get the UTF8 value of the hex string
                file.WriteLine("[BE] Showing value in UTF8: " + Encoding.UTF8.GetString(dBytes));
                file.WriteLine("[LE] Showing value in UTF8: " + Encoding.UTF8.GetString(dLBytes));

                //To get the UTF32 value of the hex string
                file.WriteLine("[BE] Showing value in UTF32: " + Encoding.UTF32.GetString(dBytes));
                file.WriteLine("[LE] Showing value in UTF32: " + Encoding.UTF32.GetString(dLBytes));

                //To get the Unicode value of the hex string
                file.WriteLine("[BE] Showing value in Unicode: " + Encoding.Unicode.GetString(dBytes));
                file.WriteLine("[LE] Showing value in Unicode: " + Encoding.Unicode.GetString(dLBytes));

                //To get the Unicode Big endian value of the hex string
                file.WriteLine("[BE] Showing value in Unicode Big Endian: " + Encoding.BigEndianUnicode.GetString(dBytes));
                file.WriteLine("[LE] Showing value in Unicode Big Endian: " + Encoding.BigEndianUnicode.GetString(dLBytes));

                foreach (EncodingInfo ei in Encoding.GetEncodings())
                {
                    //To get the codepage string value
                    file.WriteLine("[BE] Showing value in Codepage(" + ei.Name + "): " + ei.GetEncoding().GetString(dBytes));
                    file.WriteLine("[LE] Showing value in Codepage(" + ei.Name + "): " + ei.GetEncoding().GetString(dLBytes));
                }
            }

            Console.WriteLine("Done. Saved in " + filename + "\nPress any button to quit...");
            Console.ReadKey();
        }
    }
}