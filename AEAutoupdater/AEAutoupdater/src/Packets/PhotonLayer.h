#pragma once
#include "../Utils/Error.h"
#include "PhotonCommandLayer.h"
#include "LayerType.h"
#include "../Utils/BinaryReader.h"

namespace AEAutoupdater
{
	namespace Packets
	{
		const int PhotonCommandHeaderLength = 12;

		typedef struct PhotonLayer 
		{
			// Header
			uint16_t PeerId;
			uint8_t CrcEnabled;
			uint8_t CommandCount;
			uint32_t Timestamp;
			int32_t Challenge;

			// Commands
			std::vector<PhotonCommand> Commands;

			// Interface stuff
			std::vector<uint8_t> contents;
			std::vector<uint8_t> payload;

			inline LayerType LayerType() { return PhotonLayerType; }
			inline std::vector<uint8_t> LayerContents() { return contents; }
			inline std::vector<uint8_t> LayerPayload() { return payload; }
		} PhotonLayer;

		bool DecodePhotonPacket(std::vector<uint8_t> data, PhotonLayer& outLayer);
	}
}