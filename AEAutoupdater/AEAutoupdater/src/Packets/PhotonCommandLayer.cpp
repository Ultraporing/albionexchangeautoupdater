#include <iostream>
#include "PhotonCommandLayer.h"
#include "..\Utils\BinaryReader.h"


using namespace AEAutoupdater::Packets;
using namespace AEAutoupdater::Utils;

typedef enum MessageTypes
{
	OperationRequest = 2,
	OtherOperationResponse,
	EventDataType,
	OperationResponse = 7
} MessageTypes;

 bool PhotonCommand::GetReliableMessage(ReliableMessage& outMsg)
{
	ReliableMessage relmsg = {};
	BinaryReader br = BinaryReader(&Data);

	if (Type != SendReliableType)
	{
		Error(ERROR_TYPE::PCL_CONVERT, "Command can't be converted", CRITICAL);
		return false;
	}

	if (!br.Read(&relmsg.Signature) || !br.Read(&relmsg.Type))
		return false;

	if (relmsg.Type == OtherOperationResponse)
		relmsg.Type = OperationResponse;

	switch (relmsg.Type)
	{
	case OperationRequest:
		if (!br.Read(&relmsg.OperationCode))
			return false;
		break;
	case EventDataType:
		if (!br.Read(&relmsg.EventCode))
			return false;
		break;
	case OperationResponse:
		if (!br.Read(&relmsg.OperationCode) || !br.Read(&relmsg.OperationResponseCode) || !br.Read(&relmsg.OperationDebugByte))
			return false;
		break;
	default:
		Error(ERROR_TYPE::PCL_UNKNOWN_OPERATION, "Operation not recognized!", CRITICAL);
		return false;
	}

	if (!br.Read(&relmsg.ParamaterCount))
		return false;

	// Read to end
	relmsg.Data = std::vector<uint8_t>(br.NumBytesLeft());
	if (!br.Read(relmsg.Data.data(), br.NumBytesLeft()))
		return false;

	outMsg = relmsg;

	return true;
}

 bool PhotonCommand::GetReliableFragment(ReliableFragment& outFrag)
{
	ReliableFragment relmsg = {};
	BinaryReader br = BinaryReader(&Data);

	if (Type != SendReliableFragmentType)
	{
		Error(ERROR_TYPE::PCL_CONVERT, "Command can't be converted", CRITICAL);

		return false;
	}

	if (!br.Read(&relmsg.SequenceNumber) || !br.Read(&relmsg.FragmentCount) ||
		!br.Read(&relmsg.FragmentNumber) || !br.Read(&relmsg.TotalLength) ||
		!br.Read(&relmsg.FragmentOffset))
	{
		return false;
	}

	// Read to end
	relmsg.Data = std::vector<uint8_t>(br.NumBytesLeft());
	if (!br.Read(relmsg.Data.data(), br.NumBytesLeft()))
		return false;

	outFrag = relmsg;

	return true;
}
