#pragma once
#include <map>
#include "PhotonCommandLayer.h"

namespace AEAutoupdater
{
	namespace Packets
	{
		typedef struct FragmentBufferEntry
		{
			int32_t FragmentsNeeded;
			std::map<int32_t, std::vector<uint8_t>>	Fragments;

			FragmentBufferEntry();
			bool Finished();
			PhotonCommand Make();
		} FragmentBufferEntry;

		typedef struct FragmentBuffer
		{
			std::map<int32_t, FragmentBufferEntry> Entries;

			FragmentBuffer();
			bool Offer(ReliableFragment msg, PhotonCommand& outCommand);
		} FragmentBuffer;
	}
}