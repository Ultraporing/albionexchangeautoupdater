#pragma once
#include <boost/variant.hpp>
#include <vector>

namespace AEAutoupdater
{
	namespace Packets
	{
		typedef boost::variant<uint8_t, int8_t, float, double, int32_t, int16_t, int64_t, std::string, bool> SingleVariantType;
		const std::string SingleVariantTypeStrings[] = { "uint8", "int8", "float", "double", "int32", "int16", "int64", "string", "bool" };
		struct SingleVariantTypeAsStringVisitor : boost::static_visitor<std::string>
		{
			std::string operator()(int8_t i) const { return std::to_string(i); }
			std::string operator()(uint8_t i) const { return std::to_string(i); }
			std::string operator()(float f) const { return std::to_string(f); }
			std::string operator()(double d) const { return std::to_string(d); }
			std::string operator()(int32_t i) const { return std::to_string(i); }
			std::string operator()(int16_t i) const { return std::to_string(i); }
			std::string operator()(int64_t i) const { return std::to_string(i); }
			std::string operator()(std::string const& s) const { return s; }
			std::string operator()(bool b) const { return std::to_string(b); }
		};

		typedef boost::variant<std::vector<uint8_t>, std::vector<int8_t>, std::vector<float>, std::vector<double>, std::vector<int32_t>, std::vector<int16_t>, 
			std::vector<int64_t>, std::vector<std::string>, std::vector<bool>> ArrayVariantType;
		const std::string ArrayVariantTypeStrings[] = { "[uint8]", "[int8]", "[float]", "[double]", "[int32]", "[int16]", "[int64]", "[string]", "[bool]" };
		struct ArrayVariantTypeAsStringVisitor : boost::static_visitor<std::string>
		{
			std::string operator()(std::vector<uint8_t> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(SingleVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<int8_t> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(SingleVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<float> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(SingleVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<double> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(SingleVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<int32_t> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(SingleVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<int16_t> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(SingleVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<int64_t> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(SingleVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<std::string> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(v[i]);

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<bool> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(SingleVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}
		};

		typedef boost::variant<std::vector<std::vector<uint8_t>>, std::vector<std::vector<int8_t>>, std::vector<std::vector<float>>, std::vector<std::vector<double>>, 
			std::vector<std::vector<int32_t>>, std::vector<std::vector<int16_t>>, std::vector<std::vector<int64_t>>, std::vector<std::vector<std::string>>,	std::vector<std::vector<bool>>> DoubleArrayVariantType;
		const std::string DoubleArrayVariantTypeStrings[] = { "[][uint8]", "[][int8]", "[][float]", "[][double]", "[][int32]", "[][int16]", "[][int64]", "[][string]", "[][bool]" };
		struct DoubleArrayVariantTypeAsStringVisitor : boost::static_visitor<std::string>
		{
			std::string operator()(std::vector<std::vector<uint8_t>> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(ArrayVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<std::vector<int8_t>> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(ArrayVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<std::vector<float>> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(ArrayVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<std::vector<double>> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(ArrayVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<std::vector<int32_t>> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(ArrayVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<std::vector<int16_t>> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(ArrayVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<std::vector<int64_t>> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(ArrayVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<std::vector<std::string>> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(ArrayVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}

			std::string operator()(std::vector<std::vector<bool>> v) const
			{
				std::string s = std::string();

				for (int i = 0; i < v.size(); i++)
				{
					if (i == 0)
					{
						s.append("{");
					}

					s.append(ArrayVariantTypeAsStringVisitor()(v[i]));

					if (i < v.size() - 1)
						s.append(",");
					else
						s.append("}");
				}

				return s;
			}
		};

		typedef boost::make_recursive_variant<uint8_t, int8_t, float, double, int32_t, int16_t, int64_t, std::string, bool, std::vector<uint8_t>, std::vector<boost::recursive_variant_>>::type VariantType;
		struct VariantTypeAsStringVisitor : boost::static_visitor<std::string>
		{
			std::string operator()(int8_t& i) const { return std::to_string(i); }
			std::string operator()(uint8_t& i) const { return std::to_string(i); }
			std::string operator()(float& f) const { return std::to_string(f); }
			std::string operator()(double& d) const { return std::to_string(d); }
			std::string operator()(int32_t& i) const { return std::to_string(i); }
			std::string operator()(int16_t& i) const { return std::to_string(i); }
			std::string operator()(int64_t& i) const { return std::to_string(i); }
			std::string operator()(std::string const& s) const { return s; }
			std::string operator()(bool& b) const { return std::to_string(b); }

			std::string operator()(std::vector<uint8_t> v) const
			{
				std::string s = std::string("{");
				for (int i = 0; i < v.size(); i++)
				{
					s.append(std::to_string(v[i]));

					if (i < v.size() - 1)
						s.append(",");
				}
				s.append("}");

				return s;
			}
			std::string operator()(std::vector<VariantType> v) const
			{
				std::string s = std::string("{");

				for (int i = 0; i < v.size(); i++)
				{
					s.append(boost::apply_visitor(VariantTypeAsStringVisitor(), v[i]));

					if (i < v.size() - 1)
						s.append(",");
				}
				s.append("}");

				return s;
			}
		};
	}
}