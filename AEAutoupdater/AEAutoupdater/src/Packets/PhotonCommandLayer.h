#pragma once
#include <vector>

namespace AEAutoupdater
{
	namespace Packets
	{
		typedef enum CommandTypes
		{
			AcknowledgeType = 1,
			ConnectType,
			VerifyConnectType,
			DisconnectType,
			PingType,
			SendReliableType,
			SendUnreliableType,
			SendReliableFragmentType
		} CommandTypes;

		typedef struct ReliableMessage
		{
			// Header
			uint8_t Signature;
			uint8_t Type;

			// OperationRequest
			uint8_t OperationCode;

			// EventData
			uint8_t EventCode;

			// OperationResponse
			uint16_t OperationResponseCode;
			uint8_t OperationDebugByte;

			uint16_t ParamaterCount;
			std::vector<uint8_t> Data;
		} ReliableMessage;

		typedef struct ReliableFragment
		{
			int32_t SequenceNumber;
			int32_t FragmentCount;
			int32_t FragmentNumber;
			int32_t TotalLength;
			int32_t FragmentOffset;

			std::vector<uint8_t> Data;
		} ReliableFragment;

		typedef struct PhotonCommand
		{
			// Header
			uint8_t Type;
			uint8_t ChannelID;
			uint8_t Flags;
			uint8_t ReservedByte;
			int32_t Length;
			int32_t ReliableSequenceNumber;

			// Body
			std::vector<uint8_t> Data;

			bool GetReliableMessage(ReliableMessage& outMsg);
			bool GetReliableFragment(ReliableFragment& outFrag);
		} PhotonCommand;
	}
}