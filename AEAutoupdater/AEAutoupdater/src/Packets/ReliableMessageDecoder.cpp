#include "ReliableMessageDecoder.h"
#include "PhotonCommandLayer.h"
#include <sstream>

using namespace AEAutoupdater::Packets;

bool ReliableMessageDecoder::DecodeReliableMessage(ReliableMessage* msg, ReliableMessageParametersMap& outMap)
{
	Utils::BinaryReader reader = Utils::BinaryReader(&msg->Data);
	for (int i = 0; i < msg->ParamaterCount; i++)
	{
		uint8_t paramID;
		uint8_t paramType;

		if (!reader.Read(&paramID) || !reader.Read(&paramType))
			return false;

		switch (paramType)
		{
		case GpType::Unknown:
		case GpType::Null:
			break;
		case GpType::Byte:
		{
			uint8_t val;
			if (!DecodeSingleValueParam(reader, &val) || !PutParameterIntoMap(paramID, ReliableMessageParameter(val)))
				return false;
			break;
		}
		case GpType::Float:
		{
			float val;
			if (!DecodeSingleValueParam(reader, &val) || !PutParameterIntoMap(paramID, ReliableMessageParameter(val)))
				return false;
			break;
		}
		case GpType::Integer:
		{
			int32_t val;
			if (!DecodeSingleValueParam(reader, &val) || !PutParameterIntoMap(paramID, ReliableMessageParameter(val)))
				return false;
			break;
		}
		case GpType::Short:
		case 7:
		{
			int16_t val;
			if (!DecodeSingleValueParam(reader, &val) || !PutParameterIntoMap(paramID, ReliableMessageParameter(val)))
				return false;
			break;
		}
		case GpType::Long:
		{
			int64_t val;
			if (!DecodeSingleValueParam(reader, &val) || !PutParameterIntoMap(paramID, ReliableMessageParameter(val)))
				return false;
			break;
		}
		case GpType::String:
		{
			std::string strval = std::string();

			if (!DecodeSingleValueParam(reader, &strval))
				return false;
			if (!PutParameterIntoMap(paramID, ReliableMessageParameter(strval)))
				return false;
			break;
		}
		case GpType::Boolean:
		{
			bool val;
			if (!DecodeSingleValueParam(reader, &val) || !PutParameterIntoMap(paramID, ReliableMessageParameter(val)))
				return false;
			break;
		}
		case GpType::ByteArray:
		{
			std::vector<uint8_t> val;
			if (!DecodeArrayByteParam(reader, &val) || !PutParameterIntoMap(paramID, ReliableMessageParameter(val)))
				return false;
			break;
		}
		case GpType::Array:
		{
			std::vector<VariantType> val;
			if (!DecodeArrayParam(reader, &val) || !PutParameterIntoMap(paramID, ReliableMessageParameter(val)))
				return false;
			break;
		}
		default:
			std::stringstream ss;
			ss << "Invalid type of " << std::to_string(paramType) << " Current paramID " << std::to_string(paramID);
			AEAutoupdater::Utils::Error(AEAutoupdater::Utils::RMD_INVALID_VALUE, ss.str(), AEAutoupdater::Utils::CRITICAL);
			return false;
		}
	}

	outMap = ReliableMessageParameters;

	return true;
}

bool ReliableMessageDecoder::DecodeArrayByteParam(AEAutoupdater::Utils::BinaryReader& reader, std::vector<uint8_t>* outVal)
{
	uint32_t len;
	if (!DecodeSingleValueParam(reader, &len))
		return false;

	std::vector<uint8_t> arr = std::vector<uint8_t>();
	for (int i = 0; i < len; i++)
	{
		uint8_t v;
		if (!DecodeSingleValueParam(reader, &v))
			return false;

		arr.push_back(v);
	}

	*outVal = arr;
	return true;
}