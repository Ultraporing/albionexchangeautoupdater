#include "PhotonLayer.h"

using namespace AEAutoupdater::Packets;
using namespace AEAutoupdater::Utils;

bool AEAutoupdater::Packets::DecodePhotonPacket(std::vector<uint8_t> data, PhotonLayer& outLayer)
{
	PhotonLayer layer = PhotonLayer();
	BinaryReader br = BinaryReader(&data);

	if (!br.Read(&layer.PeerId) ||
		!br.Read(&layer.CrcEnabled) ||
		!br.Read(&layer.CommandCount) ||
		!br.Read(&layer.Timestamp) ||
		!br.Read(&layer.Challenge))
	{
		return false;
	}
	
	std::vector<PhotonCommand> commands = std::vector<PhotonCommand>();

	for (int i = 0; i < layer.CommandCount; i++)
	{
		PhotonCommand command = PhotonCommand();

		// Command header
		if (!br.Read(&command.Type) ||
			!br.Read(&command.ChannelID) ||
			!br.Read(&command.Flags) ||
			!br.Read(&command.ReservedByte) ||
			!br.Read(&command.Length) ||
			!br.Read(&command.ReliableSequenceNumber))
		{
			return false;
		}

		// Command Data
		int dataLength = command.Length - PhotonCommandHeaderLength;

		if (dataLength > br.BufferSize())
		{
			Error(ERROR_TYPE::PL_MALFORMED_DATA, "Data is malformed!", CRITICAL);

			return false;
		}

		command.Data = std::vector<uint8_t>(dataLength);
		if (!br.Read(command.Data.data(), dataLength))
		{
			return false;
		}

		commands.push_back(command);
	}

	layer.Commands = commands;

	// Split and store the read and unread data
	BinaryReader br2 = BinaryReader(&data);
	layer.contents = std::vector<uint8_t>(br.ReaderPos());
	if (!br2.Read(layer.contents.data(), br.ReaderPos()))
	{
		return false;
	}

	layer.payload = std::vector<uint8_t>(br.NumBytesLeft());
	if (!br2.Read(layer.payload.data(), br.NumBytesLeft()))
	{
		return false;
	}

	outLayer = layer;

	return true;
}
