#pragma once
#include <map>
#include <sstream>
#include "../Utils/BinaryReader.h"
#include "VariantType.h"

namespace AEAutoupdater
{
	namespace Packets
	{
		struct ReliableMessage;

		typedef enum GpType
		{
			Unknown,
			Array = 121,
			Boolean = 111,
			Byte = 98,
			ByteArray = 120,
			ObjectArray = 122,
			Short = 107,
			Float = 102,
			Dictionary = 68,
			Double = 100,
			Hashtable = 104,
			Integer,
			IntegerArray = 110,
			Long = 108,
			String = 115,
			StringArray = 97,
			Custom = 99,
			Null = 42,
			EventData = 101,
			OperationRequest = 113,
			OperationResponse = 112
		} GpType;

		class ReliableMessageParameter
		{
			VariantType Value;
			bool InUse = false;

		public:
			VariantType GetValue()
			{
				return Value;
			}

			std::string GetTypeName()
			{
				return "";
				/*
				int v = Value.which();
				if (v == 9)
				{
					std::stringstream ss;
					ss << VarianTypeStrings[v] << "[";
					std::vector<VarianType> vt = boost::get<std::vector<VarianType>>(Value);
					decltype(vt)::value_type p;
					int i = p.which();
					ss << VarianTypeStrings[i] << "]";

					return ss.str();
				}
				else
				{
					return VarianTypeStrings[v];
				}*/
			}

			bool IsEmpty()
			{
				return !InUse;
			}

			ReliableMessageParameter() { }
			ReliableMessageParameter(VariantType value)
			{ 
				Value = value;
				InUse = true;
			}
		};

		typedef std::map<uint8_t, ReliableMessageParameter> ReliableMessageParametersMap;

		class ReliableMessageDecoder
		{
			ReliableMessageParametersMap ReliableMessageParameters = ReliableMessageParametersMap();
			inline bool PutParameterIntoMap(uint8_t key, ReliableMessageParameter value)
			{
				auto ret = ReliableMessageParameters.insert(std::pair<uint8_t, ReliableMessageParameter>(key, value));
				
				return ret.second;
			}

		public:
			ReliableMessageDecoder() {}
			~ReliableMessageDecoder() = default;
			bool DecodeReliableMessage(ReliableMessage * msg, ReliableMessageParametersMap & outMap);
			bool DecodeArrayByteParam(AEAutoupdater::Utils::BinaryReader & reader, std::vector<uint8_t>* outVal);

			template<typename T>
			inline bool DecodeSingleValueParam(AEAutoupdater::Utils::BinaryReader& reader, T* outVal)
			{
				if (typeid(T) == typeid(uint8_t) || typeid(T) == typeid(int8_t) ||
					typeid(T) == typeid(uint16_t) || typeid(T) == typeid(int16_t) ||
					typeid(T) == typeid(uint32_t) || typeid(T) == typeid(int32_t) ||
					typeid(T) == typeid(uint64_t) || typeid(T) == typeid(int64_t) ||
					typeid(T) == typeid(float) || typeid(T) == typeid(double))
				{
					if (!reader.Read(outVal))
						return false;

					return true;
				}
				else if (typeid(T) == typeid(std::string))
				{
					uint16_t len;
					if (!reader.Read(&len))
						return false;

					std::string s = std::string();
					s.resize(len);
					if (!reader.Read(s.data(), len))
						return false;

					*((std::string*)outVal) = s;

					return true;
				}
				else if (typeid(T) == typeid(bool))
				{
					uint8_t val;
					if (!reader.Read(&val))
						return false;

					if (val == 0)
					{
						*((bool*)outVal) = false;

						return true;
					}
					else if (val == 1)
					{
						*outVal = true;

						return true;
					}
					else
					{
						AEAutoupdater::Utils::Error(AEAutoupdater::Utils::RMD_INVALID_VALUE, std::string("Invalid value for boolean of %d", val), AEAutoupdater::Utils::CRITICAL);

						return false;
					}
				}

				AEAutoupdater::Utils::Error(AEAutoupdater::Utils::RMD_INVALID_VALUE, std::string("Unknown Type for Single Value Decoder"), AEAutoupdater::Utils::CRITICAL);
				return false;
			}

			template<typename T>
			inline bool DecodeArrayParam(AEAutoupdater::Utils::BinaryReader& reader, T* outVal)
			{
				uint16_t len;
				if (reader.Read(&len))
					return false;

				uint8_t arrType;
				if (reader.Read(&arrType))
					return false;

				switch (arrType)
				{
				case GpType::Float:
				{
					std::vector<float> arr = std::vector<float>(len);
					for (int i = 0; i < len; i++)
					{
						float v;
						if (DecodeSingleValueParam(reader, &v))
							return false;

						arr.push_back(v);
					}

					*((std::vector<float>*)outVal) = arr;
					return true;
				}
				case GpType::Integer:
				{
					std::vector<int32_t> arr = std::vector<int32_t>(len);
					for (int i = 0; i < len; i++)
					{
						int32_t v;
						if (DecodeSingleValueParam(reader, &v))
							return false;

						arr.push_back(v);
					}

					*((std::vector<int32_t>*)outVal) = arr;
					return true;
				}
				case GpType::Short:
				{
					std::vector<int16_t> arr = std::vector<int16_t>(len);
					for (int i = 0; i < len; i++)
					{
						int16_t v;
						if (DecodeSingleValueParam(reader, &v))
							return false;

						arr.push_back(v);
					}

					*((std::vector<int16_t>*)outVal) = arr;
					return true;
				}
				case GpType::Long:
				{
					std::vector<int64_t> arr = std::vector<int64_t>(len);
					for (int i = 0; i < len; i++)
					{
						int64_t v;
						if (DecodeSingleValueParam(reader, &v))
							return false;

						arr.push_back(v);
					}

					*((std::vector<int64_t>*)outVal) = arr;
					return true;
				}
				case GpType::String:
				{
					std::vector<std::string> arr = std::vector<std::string>(len);
					for (int i = 0; i < len; i++)
					{
						std::string v;
						if (DecodeSingleValueParam(reader, &v))
							return false;

						arr.push_back(v);
					}

					*((std::vector<std::string>*)outVal) = arr;
					return true;
				}
				case GpType::Boolean:
				{
					std::vector<bool> arr = std::vector<bool>(len);
					for (int i = 0; i < len; i++)
					{
						bool v;
						if (DecodeSingleValueParam(reader, &v))
							return false;

						arr.push_back(v);
					}

					*((std::vector<bool>*)outVal) = arr;
					return true;
				}
				case GpType::ByteArray:
				{
					std::vector<std::vector<uint8_t>> arr = std::vector<std::vector<uint8_t>>(len);
					for (int i = 0; i < len; i++)
					{
						std::vector<uint8_t> v;
						if (DecodeArrayByteParam(reader, &v))
							return false;

						arr.push_back(v);
					}

					*((std::vector<std::vector<uint8_t>>*)outVal) = arr;
					return true;
				}
				case GpType::Array:
				{
					std::vector<std::vector<void*>> arr = std::vector<std::vector<void*>>(len);
					for (int i = 0; i < len; i++)
					{
						std::vector<void*> v;
						if (DecodeArrayParam(reader, &v))
							return false;

						arr.push_back(v);
					}

					*((std::vector<std::vector<void*>>*)outVal) = arr;
					return true;
				}
				default:
					AEAutoupdater::Utils::Error(AEAutoupdater::Utils::RMD_INVALID_VALUE, std::string("Invalid array type of %d", arrType), AEAutoupdater::Utils::CRITICAL);
					return false;
				}


				return false;
			}
		};	

	}
}

