#include "FragmentBuffer.h"

using namespace AEAutoupdater::Packets;

FragmentBufferEntry::FragmentBufferEntry()
{
	FragmentsNeeded = 0;
	Fragments = std::map<int32_t, std::vector<uint8_t>>();
}

bool FragmentBufferEntry::Finished()
{
	return Fragments.size() == FragmentsNeeded;
}

PhotonCommand FragmentBufferEntry::Make()
{
	PhotonCommand cmd = PhotonCommand();
	cmd.Type = SendReliableType;
	cmd.Data = std::vector<uint8_t>();

	for (int i = 0; i < FragmentsNeeded; i++)
	{
		cmd.Data.insert(cmd.Data.end(), Fragments[i].begin(), Fragments[i].end());
	}

	return cmd;
}

FragmentBuffer::FragmentBuffer()
{
	Entries = std::map<int32_t, FragmentBufferEntry>();
}

bool FragmentBuffer::Offer(ReliableFragment msg, PhotonCommand& outCommand)
{
	FragmentBufferEntry entry = FragmentBufferEntry();

	if (Entries.find(msg.SequenceNumber) != Entries.end())
	{
		entry = Entries.at(msg.SequenceNumber);
		entry.Fragments[msg.FragmentNumber] = msg.Data;
	}
	else
	{
		entry.FragmentsNeeded = msg.FragmentCount;
		entry.Fragments[msg.FragmentNumber] = msg.Data;
	}

	if (entry.Finished())
	{
		PhotonCommand cmd = entry.Make();
		entry.Fragments.erase(msg.FragmentNumber);

		outCommand = cmd;

		return true;
	}
	else
	{
		Entries.insert(std::pair<int32_t, FragmentBufferEntry>(msg.SequenceNumber, entry));
	}

	return false;
}

