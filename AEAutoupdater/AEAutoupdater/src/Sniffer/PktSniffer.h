#pragma once
#include <tins/tins.h>
#include "../Packets/PhotonLayer.h"
#include "Packets\ReliableMessageDecoder.h"
#include "../Packets/PhotonCommandLayer.h"

namespace AEAutoupdater
{
	namespace Sniffer
	{
		class PktSniffer
		{
		public:
			PktSniffer();
			PktSniffer::PktSniffer(std::string pcapFile);
			~PktSniffer();
			void Start();
			void Stop();
			bool IsRunning();

		private:
			bool m_running;
			bool Callback(const Tins::PDU &pdu);
			void StartSniffing();
			Tins::SnifferConfiguration InitSnifferConfig();
			Tins::BaseSniffer* m_sniffer;
		};
	}
}