#include "PktSniffer.h"
#include <iostream>
#include <functional>
#include <conio.h>
#include <thread>
#include <sstream>
#include <locale>
#include <codecvt>
#include <windows.h>
#include <string>
#include <fstream>


using namespace AEAutoupdater::Sniffer;

Tins::NetworkInterface iface;
std::string friendlyInterfaceName;

const int AlbionOnline_Gameserver_Port = 5056;

bool dirExists(const std::string& dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;  //something is wrong with your path!

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;   // this is a directory!

	return false;    // this is not a directory!
}

void DeleteAllFiles(char* folderPath)
{
	char fileFound[256];
	WIN32_FIND_DATA info;
	HANDLE hp;
	sprintf(fileFound, "%s\\*.*", folderPath);
	hp = FindFirstFile(fileFound, &info);
	do
	{
		sprintf(fileFound, "%s\\%s", folderPath, info.cFileName);
		DeleteFile(fileFound);

	} while (FindNextFile(hp, &info));
	FindClose(hp);
}

void setupDumpDirectory()
{
	// Setup packet dump dir ONLY FOR DEBUGGING NOT IN FINAL PRODUCT!
	if (!dirExists("PktDump"))
	{
		CreateDirectory("PktDump", NULL);
	}
	else
	{
		DeleteAllFiles("PktDump");
	}
}

std::string ExePath() {
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");
	return std::string(buffer).substr(0, pos);
}

int pktCounter = 0;
void createPktDumpFile(std::vector<uint8_t> pkt)
{
	std::stringstream filename;
	filename << ExePath() << "\\PktDump\\" << pktCounter << ".bin";
	std::fstream myFile(filename.str().c_str(), std::fstream::out | std::fstream::binary);
	if (myFile.is_open())
	{
		myFile.write((char*)&pkt[0], pkt.size() * sizeof(uint8_t));
		myFile.close();
		pktCounter++;
	}
	else
	{
		std::cout << "Failed to open File " << filename.str().c_str() << std::endl;
	}
	
}

PktSniffer::PktSniffer()
{
	// Create InterfaceSniffer with config
	m_sniffer = new Tins::Sniffer(iface.name(), InitSnifferConfig());
}

PktSniffer::PktSniffer(std::string pcapFile)
{
	// Create FileSniffer with config
	m_sniffer = new Tins::FileSniffer(pcapFile, InitSnifferConfig());
}

Tins::SnifferConfiguration PktSniffer::InitSnifferConfig()
{
	setupDumpDirectory();

	m_running = false;
	iface = Tins::NetworkInterface::default_interface();

	// setup wstring converter
	using convert_type = std::codecvt_utf8<wchar_t>;
	std::wstring_convert<convert_type, wchar_t> converter;

	//use converter to get our friendly interface name as string instead of wstring
	friendlyInterfaceName = converter.to_bytes(iface.friendly_name());

	// Construct our filter string
	std::stringstream ss;
	ss << "src port " << AlbionOnline_Gameserver_Port;

	// create a config and set Filter
	Tins::SnifferConfiguration snifConf;
	snifConf.set_filter(ss.str().c_str());

	return snifConf;
}

PktSniffer::~PktSniffer()
{
	if (m_sniffer)
	{
		delete m_sniffer;
		m_sniffer = NULL;
	}
}

// our sniffing thread
std::thread t;

// Start Button Callback, To start sniffing
void PktSniffer::Start()
{
	if (!IsRunning())
	{
		std::cout << "Sniffing started at NetworkInterface (" << friendlyInterfaceName.c_str() << ")" << std::endl;
		m_running = true;

		// Create a new thread for the sniffing loop
		t = std::thread(std::bind(&PktSniffer::StartSniffing, this));
	}
}

// Thread Function calling the blocking sniffing loop
void PktSniffer::StartSniffing()
{
	m_sniffer->sniff_loop(std::bind(&PktSniffer::Callback, this, std::placeholders::_1));
}

// Stop Button Callback, To stop sniffing and join thread
void PktSniffer::Stop()
{
	if (IsRunning())
	{
		std::cout << "Sniffing stopped" << std::endl;
		m_running = false;
		m_sniffer->stop_sniff();
		t.join();
	}
}

bool PktSniffer::IsRunning()
{
	return m_running;
}

// Sniffing callback, here get all packets handled
bool PktSniffer::Callback(const Tins::PDU & pdu)
{
	// Get IP Layer
	const Tins::IP &ip = pdu.rfind_pdu<Tins::IP>();
	// Get UDP Layer
	const Tins::UDP &udp = pdu.rfind_pdu<Tins::UDP>();
	// Get RawPDU from UDP Layer
	const Tins::RawPDU &rawPdu = udp.rfind_pdu<Tins::RawPDU>();
	// Get the Payload from rawPdu
	const Tins::RawPDU::payload_type &payload = rawPdu.payload();

	std::cout << ip.src_addr() << ':' << udp.sport() << " -> " << ip.dst_addr() << ':' << udp.dport() << "[" << udp.length() << "]\n";

	Packets::PhotonLayer layer = Packets::PhotonLayer();
	if (!Packets::DecodePhotonPacket(payload, layer))
		return false;

	Packets::ReliableMessageDecoder dec = Packets::ReliableMessageDecoder();
	Packets::ReliableMessageParametersMap m = Packets::ReliableMessageParametersMap();

	for (int i = 0; i < layer.CommandCount; i++)
	{
		Packets::ReliableMessage msg;
		if (!layer.Commands.at(i).GetReliableMessage(msg) || !dec.DecodeReliableMessage(&msg, m))
			return false;
	}
	

	return IsRunning();
}