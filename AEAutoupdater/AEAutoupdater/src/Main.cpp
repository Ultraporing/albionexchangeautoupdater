#include <nana\gui.hpp>
#include <nana\gui\widgets\label.hpp>
#include <nana\gui\widgets\button.hpp>
#include <iostream>
#include "Sniffer\PktSniffer.h"
#include "Utils\BinaryReader.h"
#include <fstream>

static std::vector<uint8_t> ReadAllBytes(char const* filename)
{
	std::basic_ifstream<uint8_t> ifs(filename, std::ios::binary | std::ios::ate);
	std::ifstream::pos_type pos = ifs.tellg();

	std::vector<uint8_t>  result(pos);

	ifs.seekg(0, std::ios::beg);
	ifs.read(&result[0], pos);

	return result;
}

int main(int argc, char *argv[])
{
	using namespace AEAutoupdater;

	auto a = ReadAllBytes("ra.bin");
	
	Packets::PhotonLayer layer = Packets::PhotonLayer();
	if (!Packets::DecodePhotonPacket(a, layer))
		return false;

	AEAutoupdater::Packets::ReliableMessageDecoder dec = Packets::ReliableMessageDecoder();
	Packets::ReliableMessageParametersMap m = Packets::ReliableMessageParametersMap();

	for (int i = 0; i < layer.CommandCount; i++)
	{
		Packets::ReliableMessage msg;
		if (!layer.Commands.at(i).GetReliableMessage(msg) || !dec.DecodeReliableMessage(&msg, m))
			continue;
	}

	for (int i = 0; i < m.size(); i++)
	{
		if (!m[i].IsEmpty())
		{
			Packets::VariantType v = m[i].GetValue();
			std::cout << "[" << i << "](" << m[i].GetTypeName() << "): " << boost::apply_visitor(Packets::VariantTypeAsStringVisitor(), v) << std::endl;
		}
	}

	/*
	Sniffer::PktSniffer pktS;

	if (argc == 0)
	{
		// create our packet sniffer
		pktS = Sniffer::PktSniffer();
	}
	else
	{
		// create our packet sniffer
		pktS = Sniffer::PktSniffer(std::string(argv[0]));
	}


	// create our window
	nana::form fm{ nana::API::make_center(400, 300), nana::appearance(true, true, true, false, false, false, false) };
	fm.caption("AlbionExchange Autoupdater");

	// random ass test label
	nana::label lb{ fm, nana::rectangle{ 10, 10, 100, 100 } };
	lb.caption("Hello, world!");

	// Setting up the Start Button, It'll call the Start function
	nana::button startButton{ fm, nana::rectangle{10, 40, 100, 40} };
	startButton.caption("Start Sniffer");
	startButton.events().click(std::bind(&Sniffer::PktSniffer::Start, &pktS));
	
	// Setting up the Stop Button, It'll call the Stop function
	nana::button stopButton{ fm, nana::rectangle{ 10, 85, 100, 40 } };
	stopButton.caption("Stop Sniffer");
	stopButton.events().click(std::bind(&Sniffer::PktSniffer::Stop, &pktS));

	// show window and enter window event loop
	fm.show();
	nana::exec();
	*/
	return 0;
}