#pragma once
#include <string>

namespace AEAutoupdater
{
	namespace Utils
	{
		typedef enum ERROR_TYPE
		{
			NONE,
			PCL_CONVERT,
			PCL_UNKNOWN_OPERATION,
			PL_MALFORMED_DATA,
			BR_READ_OUT_OF_BOUNDS,
			RMD_INVALID_VALUE
		} ERROR_TYPE;

		typedef enum ERROR_LEVEL
		{
			WARNING,
			CRITICAL
		}ERROR_LEVEL;

		class Error
		{
		public:
			std::string m_errorText;
			ERROR_TYPE m_errorNumber;
			ERROR_LEVEL m_errorLevel;

			Error(ERROR_TYPE err, const std::string errorText, ERROR_LEVEL errorLevel = WARNING);
			void Print();
			std::string ToString();
			bool ErrorOccurred();
		};
	}
}

