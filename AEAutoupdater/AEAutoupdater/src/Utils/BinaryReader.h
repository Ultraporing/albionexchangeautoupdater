#pragma once
#include <vector>
#include "Error.h"
#include <intrin.h>
#include <algorithm>
#include <typeinfo>

namespace AEAutoupdater
{
	namespace Utils
	{
		class BinaryReader
		{
		private:
			std::vector<uint8_t>* m_buffer;
			int m_readerPos;

		public:
			BinaryReader(std::vector<uint8_t>* data);
			inline size_t BufferSize() { return m_buffer != nullptr ? m_buffer->size() : 0; }
			inline int ReaderPos() { return m_buffer != nullptr ? m_readerPos : 0; }
			inline size_t NumBytesLeft() { return m_buffer != nullptr ? BufferSize() - ReaderPos() : 0; }

			template <class T>
			void endswap(T *objp)
			{
				unsigned char *memp = reinterpret_cast<unsigned char*>(objp);
				std::reverse(memp, memp + sizeof(T));
			}

			template<typename T> inline bool Read(T* outValue)
			{
				size_t typeSize = sizeof(T);
				T tv = T();

				if (!Read(&tv, typeSize))
				{
					return false;
				}

				if (typeid(T) == typeid(uint16_t) || typeid(T) == typeid(int16_t) ||
					typeid(T) == typeid(uint32_t) || typeid(T) == typeid(int32_t) ||
					typeid(T) == typeid(uint64_t) || typeid(T) == typeid(int64_t) ||
					typeid(T) == typeid(float) || typeid(T) == typeid(double))
				{
					endswap(&tv);
				}

				*outValue = tv;

				return true;
			}

			inline bool Read(void* outValue, size_t size)
			{
				if (m_readerPos + size > m_buffer->size())
				{
					Error(BR_READ_OUT_OF_BOUNDS, "Binary reader would read out of bounds. Canceled read!", CRITICAL);

					return false;
				}

				memcpy(outValue, m_buffer->data() + m_readerPos, size);

				m_readerPos += size;

				return true;
			}

			template<typename T>
			inline bool Peek(T* outValue, int offset)
			{
				if (m_readerPos + offset + sizeof(T) > m_buffer->size())
				{
					Error(BR_READ_OUT_OF_BOUNDS, "Binary reader would read out of bounds. Canceled read!", CRITICAL);

					return false;
				}

				T tv = T();

				memcpy(&tv, m_buffer->data() + m_readerPos + offset, sizeof(T));

				if (typeid(T) == typeid(uint16_t) || typeid(T) == typeid(int16_t) ||
					typeid(T) == typeid(uint32_t) || typeid(T) == typeid(int32_t) ||
					typeid(T) == typeid(uint64_t) || typeid(T) == typeid(int64_t) ||
					typeid(T) == typeid(float) || typeid(T) == typeid(double))
				{
					endswap(&tv);
				}

				*outValue = tv;

				return true;
			}
		};
	}
}