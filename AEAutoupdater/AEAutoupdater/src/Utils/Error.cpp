#include "Error.h"
#include <iostream>
#include <sstream>

using namespace AEAutoupdater::Utils;
#define stringify( name ) #name

const char* ErrorEnumStrings[] =
{
	stringify(NONE),
	stringify(PCL_CONVERT),
	stringify(PCL_UNKNOWN_OPERATION),
	stringify(PL_MALFORMED_DATA),
	stringify(BR_READ_OUT_OF_BOUNDS),
	stringify(RMD_INVALID_VALUE)
};

const char* ErrorLevelEnumStrings[] =
{
	stringify(WARNING),
	stringify(CRITICAL)
};

Error::Error(const ERROR_TYPE err, const std::string errorText, ERROR_LEVEL errorLevel)
{
	m_errorNumber = err;
	m_errorText = errorText;
	m_errorLevel = errorLevel;

	if (m_errorNumber > NONE)
	{
		Print();
	}
}

void Error::Print()
{
	std::cout << ToString();
}

std::string Error::ToString()
{
	std::stringstream ss;
	ss << "(" << ErrorLevelEnumStrings[m_errorLevel] << ")[" << ErrorEnumStrings[m_errorNumber] << "]: " << m_errorText.c_str() << std::endl;

	return ss.str();
}

bool Error::ErrorOccurred()
{
	return m_errorNumber != 0;
}
