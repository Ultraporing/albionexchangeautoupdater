#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "tins" for configuration "Release"
set_property(TARGET tins APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(tins PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "E:/AlbionExchangeAutoupdater/AEAutoupdater/externals/WpdPack/Lib/wpcap.lib;Ws2_32.lib;Iphlpapi.lib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/tins.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS tins )
list(APPEND _IMPORT_CHECK_FILES_FOR_tins "${_IMPORT_PREFIX}/lib/tins.lib" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
